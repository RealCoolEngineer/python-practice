import click


@click.command()
@click.argument('user-name')
@click.option('-p', '--param-name', 'custom_param_name', default='magic', help='Parameter introduce.')
@click.option('--count', type=int, prompt='Input greet times', help='The times to greet.')
@click.option('-d', '--debug', is_flag=True, help='Enable debug mode')
def main(user_name, custom_param_name, count, debug):
    if debug:
        print('Enter debug mode')
    for i in range(count):
        print(f'Greet {i}: Hello {user_name}')
    print(f'Custom parameter is: {custom_param_name}')


if __name__ == "__main__":
    main()
